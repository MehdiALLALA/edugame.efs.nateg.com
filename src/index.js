
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "assets/plugins/nucleo/css/nucleo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/scss/argon-dashboard-react.scss";

import AdminLayout from "layouts/Admin.js";
import GuestLayout from "layouts/Guest.js";
import StudentLayout from "layouts/Student.js";
import TeacherLayout from "layouts/Teacher.js";
import { Provider } from "react-redux";
import store from './store';




ReactDOM.render(
  <Provider store={ store }>
  <BrowserRouter>
    <Switch>
      <Route path="/admin" render={props => <AdminLayout {...props} />} />
      <Route path="/guest" render={props => <GuestLayout {...props} />} />
      <Route path="/student" render={props => <StudentLayout {...props} />} />
      <Route path="/teacher" render={props => <TeacherLayout {...props} />} />
      <Redirect from="/" to="/guest/login" />
    </Switch>
  </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
