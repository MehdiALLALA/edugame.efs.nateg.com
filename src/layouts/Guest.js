import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
// reactstrap components
import { Container } from "reactstrap";
// core components
import GuestNavbar from "components/Navbars/GuestNavbar.js";
import GuestFooter from "components/Footers/GuestFooter.js";
import  routes  from "routes.js";

class Guest extends React.Component {
  componentDidUpdate(e) {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.mainContent.scrollTop = 0;
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/guest") {
        return (
          <Route
            path={"/guest"+prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  render() {
    return (
      <>
        <div className="main-content" ref="mainContent">
          <GuestNavbar
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
            routes={routes}
            logo={{
                innerLink: "/guest",
                imgSrc: require("assets/img/brand/logo2.png"),
                imgAlt: "..."
            }}
          />
          <Switch>
            {this.getRoutes(routes)}
            <Redirect from="*" to="/guest" />
          </Switch>
          <Container fluid>
          <br/><br/><br/>
            <GuestFooter />
          </Container>
        </div>
      </>
    );
  }
}

export default Guest;
