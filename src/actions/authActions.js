import axios from 'axios'
import setAuthToken from '../utils/setAuthToken'
import jwt_decode from 'jwt-decode'
import { SET_CURRENT_USER } from './types'


export const loginUser = (userData) => dispatch => {
    axios.post('http://localhost:5000/users/login',userData)
    .then( res => {
      const { token } = res.data;   
      localStorage.setItem('jwtToken','Bearer '+token)
      setAuthToken('Bearer '+token);
      const decoded = jwt_decode(token);
      dispatch(setCurrentUser(decoded))
    })
    .catch(function (error) {
      console.log(error);
    });
}

export const setCurrentUser = (decoded) => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    }
}