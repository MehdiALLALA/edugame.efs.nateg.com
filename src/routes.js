/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import AdminDashboard from "views/Administrator/Dashboard.js";
import Profile from "views/Administrator/Profile.js";
import Maps from "views/Administrator/Maps.js";
import Register from "views/Guest/Register.js";
import Login from "views/Guest/Login.js";
import Tables from "views/Administrator/Tables.js";
import Icons from "views/Administrator/Icons.js";
import StudentDashboard from "views/Student/Dashboard.js";
import TeacherDashboard from "views/Teacher/Dashboard.js"
import HomeView from "views/Guest/HomeView.js";
import CoursesView from "views/Guest/CoursesView.js";

var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "ni ni-laptop",
    component: AdminDashboard,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "ni ni-planet text-blue",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/maps",
    name: "Maps",
    icon: "ni ni-pin-3 text-orange",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/user-profile",
    name: "User Profile",
    icon: "ni ni-single-02 text-yellow",
    component: Profile,
    layout: "/admin"
  },
  {
    path: "/tables",
    name: "Tables",
    icon: "ni ni-bullet-list-67 text-red",
    component: Tables,
    layout: "/admin"
  },
  // student routes
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "ni ni-laptop",
    component: StudentDashboard,
    layout: "/student"
  },

  // teacher routes

  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "ni ni-laptop",
    component: TeacherDashboard,
    layout: "/teacher"
  },
  //HomeRoutes
  {
    path: "/home",
    name: "HOME",
    component: HomeView,
    layout: "/guest"
  },
  {
    path: "/courses",
    name: "COURSES",
    component: CoursesView,
    layout: "/guest"
  },
  {
    path: "/login",
    name: "LOGIN",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/guest"
  },
  {
    path: "/register",
    name: "REGISTER",
    icon: "ni ni-circle-08 text-pink",
    component: Register,
    layout: "/guest"
  }
];
export default routes;
