import React from "react";
import logo from '../../assets/img/brand/logo2.png';

// reactstrap components
import { Container, Row, Col, Nav, NavItem, NavLink } from "reactstrap";

class GuestFooter extends React.Component {
  render() {
    return (
 <footer class="page-footer font-small stylish-color-dark pt-4">

  {/* Footer Links */}
  <div class="container text-center text-md-left">

    {/* Grid row */}
    <div class="row">

      {/* Grid column */}
      <div class="col-md-4 mx-auto">

        {/* Content */}
                <img style={{
                              marginTop :"-80px",
                            }}
                    alt="error"
                    className="footer-brand-img"
                    src={logo}
                />
        <p className="description" style={{ fontSize:"1.1vw" }}>Un texte est une série orale ou écrite de mots perçus comme constituant un ensemble cohérent, porteur de sens et utilisant les structures propres à une langue .</p>

      </div>
      {/* Grid column */}

      <hr class="clearfix w-100 d-md-none"/>

      {/* Grid column */}
      <div class="col-md-2 mx-auto">

        {/* Links */}
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">NAVIGATION</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!">Link 1</a>
          </li>
          <li>
            <a href="#!">Link 2</a>
          </li>
          <li>
            <a href="#!">Link 3</a>
          </li>
          <li>
            <a href="#!">Link 4</a>
          </li>
        </ul>

      </div>
      {/* Grid column */}

      <hr class="clearfix w-100 d-md-none"/>

      {/* Grid column */}
      <div class="col-md-2 mx-auto">

        {/* Links */}
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">COURSES</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!">Link 1</a>
          </li>
          <li>
            <a href="#!">Link 2</a>
          </li>
          <li>
            <a href="#!">Link 3</a>
          </li>
          <li>
            <a href="#!">Link 4</a>
          </li>
        </ul>

      </div>
      {/* Grid column */}

      <hr class="clearfix w-100 d-md-none"/>

      {/* Grid column */}
      <div class="col-md-2 mx-auto">

        {/* Links */}
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">CONTACT US</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!">Link 1</a>
          </li>
          <li>
            <a href="#!">Link 2</a>
          </li>
          <li>
            <a href="#!">Link 3</a>
          </li>
          <li>
            <a href="#!">Link 4</a>
          </li>
        </ul>

      </div>
      {/* Grid column */}

    </div>
    {/* Grid row */}

  </div>
  {/* Footer Links */}

  <hr/>

  {/* Call to action */}
  <ul class="list-unstyled list-inline text-center py-2">
    <li class="list-inline-item">
      <h5 class="mb-1">Register for free</h5>
    </li>
    <li class="list-inline-item">
      <a style={{ 
         backgroundColor: "#19BBAE",color: "white" }}href="#!" class="btn  btn-rounded">Sign Up !</a>
    </li>
  </ul>
  {/* Call to action */}

  <hr/>

  {/* Social buttons */}
  <ul class="list-unstyled list-inline text-center">
    <li class="list-inline-item">
      <a class="btn-floating btn-fb mx-1">
        <i class="fab fa-facebook-f"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-tw mx-1">
        <i class="fab fa-twitter"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-gplus mx-1">
        <i class="fab fa-google-plus-g"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-li mx-1">
        <i class="fab fa-linkedin-in"> </i>
      </a>
    </li>
  </ul>
  {/* Social buttons */}

  {/* Copyright */}
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href=""> IiTiI</a>
  </div>
  {/* Copyright */}

</footer>
    );
  }
}

export default GuestFooter;
