/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { NavLink as NavLinkRRD, Link } from "react-router-dom";

import { PropTypes } from "prop-types";
// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  Collapse,
  InputGroupAddon,
  InputGroupText,
  NavbarBrand,
  Input,
  InputGroup,
  Navbar,
  NavLink,
  NavItem,
  Nav,
  Container,
  Media
} from "reactstrap";



class GuestNavbar extends React.Component {
    state = {
        collapseOpen: false
      };
      constructor(props) {
        super(props);
        this.activeRoute.bind(this);
      }
      // verifies if routeName is the one active (in browser input)
      activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
      }
      // toggles collapse between opened and closed (true/false)
      toggleCollapse = () => {
        this.setState({
          collapseOpen: !this.state.collapseOpen
        });
      };
      // closes the collapse
      closeCollapse = () => {
        this.setState({
          collapseOpen: false
        });
      };
    createLinks = routes => {
        return routes.map((prop, key) => {
          if (prop.layout === "/guest"){
          return (
            <NavItem  key={key}>
              <NavLink style={{ fontWeight: "bold" }}
                to={"/guest"+prop.path}
                tag={NavLinkRRD}
                onClick={this.closeCollapse}
                activeClassName="active"
              >
                {prop.name}
                { console.log(prop.path) }

              </NavLink>
            </NavItem>
          );}
          else return null;
        });
      };
  render() {
    const { bgColor, routes, logo } = this.props;
    return (
      <>
        <Navbar  className="navbar-top navbar-dark" expand="md" id="navbar-main">
          <Container fluid>
            {logo ? (
                <NavbarBrand className="pt-1" >
                <Link to="/guest">
                <img alt={logo.imgAlt}
                    className="navbar-brand-img"
                    src={logo.imgSrc}
                /></Link>
                </NavbarBrand>
            ) : null}
            <Link
              className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block"
              to="/"
            >
             {/* {this.props.brandText} */}
            </Link>
            <Collapse navbar isOpen={this.state.collapseOpen}>
            <Nav navbar>{this.createLinks(routes)}</Nav>
            </Collapse>
            <Form className="navbar-search navbar-search-black form-inline mr-3 d-none d-md-flex ml-lg-auto">
              <FormGroup className="mb-0">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon  addonType="prepend">
                    <InputGroupText>
                      <i className="fas fa-search" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Search" type="text" />
                </InputGroup>
              </FormGroup>
            </Form>
          </Container>
        </Navbar>
      </>
    );
  }
}

GuestNavbar.defaultProps = {
    routes: [{}]
  };
  
  GuestNavbar.propTypes = {
    // links that will be displayed inside the component
    routes: PropTypes.arrayOf(PropTypes.object),
    logo: PropTypes.shape({
      // innerLink is for links that will direct the user within the app
      // it will be rendered as <Link to="...">...</Link> tag
      innerLink: PropTypes.string,
      // outterLink is for links that will direct the user outside the app
      // it will be rendered as simple <a href="...">...</a> tag
      outterLink: PropTypes.string,
      // the image src of the logo
      imgSrc: PropTypes.string.isRequired,
      // the alt for the img
      imgAlt: PropTypes.string.isRequired
    })
  };
  

export default GuestNavbar;
