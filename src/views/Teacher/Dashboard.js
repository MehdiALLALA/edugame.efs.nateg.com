import React from "react";
// node.js library that concatenates classes (strings)
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
// reactstrap components
import {
  Container,
  Row,  
  Label
} from "reactstrap";

// core components
import {
  chartOptions,
  parseOptions,
} from "variables/charts.js";

import TeacherHeader from "components/Headers/TeacherHeader.js";

class Dashboard extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      activeNav: 1,
      chartExample1Data: "data1"
    };
    if (window.Chart) {
      parseOptions(Chart, chartOptions());
    }
  }
  toggleNavs = (e, index) => {
    e.preventDefault();
    this.setState({
      activeNav: index,
      chartExample1Data:
        this.state.chartExample1Data === "data1" ? "data2" : "data1"
    });
  };
  render() {
    return (
      <>
        <TeacherHeader/>
        {/* Page content */}
        <Container className="mt--7" fluid>
        <Row>
            <div className="col"><Label className="bg-secondary">Hello Teacher</Label></div>
            
        </Row>
        </Container>
      </>
    );
  }
}

export default Dashboard;
