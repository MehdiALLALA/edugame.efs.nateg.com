import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from '../../actions/authActions'
import Header from "components/Headers/Header.js";
// reactstrap components
import {
  Button,
  Card,
  //CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
  UncontrolledCarousel,
  Container,
} from "reactstrap";

import python from '../../assets/img/brand/python.jpg';
import slide1 from '../../assets/img/brand/slide1.png';
import slide2 from '../../assets/img/brand/slide2.png';
import slide3 from '../../assets/img/brand/slide3.png';
const items = [
  {
    src: slide1,
    alt:"description of image",
    altText: '',
    caption: '',
    header: '',
    key: '1'
  },
  {
    src: slide2,
    altText: '',
    caption: '',
    header: '',
    key: '2'
  },
  {
    src: slide3,
    altText: '',
    caption: '',
    header: '',
    key: '3'
  }
];

class Login extends React.Component {
  constructor(){
    super();
    this.state = {
      email: '',
      password: '',
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e){
    this.setState({[e.target.placeholder]: e.target.value})
  }

  onSubmit(e){
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData)
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    if(nextProps.auth.isAuthenticated){
      if(nextProps.auth.user.permission === 100){
      this.props.history.push('/admin/dashboard')
      }
      if(nextProps.auth.user.permission === 1){
        this.props.history.push('/student/dashboard')
      }
      if(nextProps.auth.user.permission === 2){
        this.props.history.push('/teacher/dashboard')
      }
    }
  }

  render() {
    return (
      <>
        <Header />
        <h1 style={{ fontSize:"3vw",marginTop: '5%'}} className="col text-center">LOGIN</h1>
        <Col lg="5" md="7"  style={{  marginTop: '5%',marginLeft:"30%",marginBlockEnd: '5%'
                    }} >
                      
          <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5">
              <Form role="form" onSubmit={this.onSubmit}>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="email" type="email" value={this.state.email} onChange={this.onChange}/>
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="password" type="password" value={this.state.password} onChange={this.onChange}/>
                  </InputGroup>
                </FormGroup>
                <div className="text-center">
                  <Button className="my-4" color="primary" type="submit">
                    Sign in
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Forgot password?</small>
              </a>
            </Col>
            <Col className="text-right" xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Create new account</small>
              </a>
            </Col>
          </Row>
        </Col>
        <Container  fluid style={{ backgroundColor: '#EAEDF1' }}>
          <Row>
            <div className=" col" style={{ marginTop: '5%',marginLeft:"10%",marginRight:"10%",marginBlockEnd: '5%' }}>
              <Row>
                <Col >
                  <card >
                    <h5 style={{ fontSize:"2vw",textAlign: "center" }} className="title">
                    “A journey of a thousand miles begins with a single step.  
                    Work for what you want, where you want to be and who you want to be. 
                    Success does not fall into your lap by doing nothing. 
                    With dedication and hard work, you can achieve anything. 
                    The beginning to the rest of your life starts here…”
                    </h5>
                  </card>
                </Col>
              </Row>
            </div>
          </Row>
        </Container>

        <div  style={{ backgroundColor: '#F8F9FE' }}>
          <br/><br/>
          <h1 style={{ fontSize:"3vw",textAlign: "center" }} >Why Eduplay works ?</h1>
          <br/><br/>
          <div style={{  marginLeft:"10%",marginRight:"10%" }}>
          <UncontrolledCarousel  items={items} />
          </div>
        </div>
        
        <br/><br/><br/>

        <Container  fluid style={{ backgroundColor: '#EAEDF1' }}>
          <Row>
            <div className=" col" style={{ marginTop: '2.5%',marginBlockEnd: '3%' }}>
              <Row>
                <Col >
                  <card >
                    <h5 style={{ fontSize:"3vw",textAlign: "center" }} className="title">
                    Become a member of our growing community !
                    </h5>
                    <div className="col text-center">
                      <Button 
                      style={{ 
                        backgroundColor: "#19BBAE",color: "white",width : "250px",borderColor: '#19BBAE' }}
                        href="/"
                        target="_blank"
                      >
                        Start Learning Now
                      </Button>
                    </div>
                  </card>
                </Col>
              </Row>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth
  //errors: state.errors
})

export default connect(mapStateToProps, { loginUser })(Login);
