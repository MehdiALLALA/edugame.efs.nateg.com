import React from "react";
// react component that copies the given text inside your clipboard
import { CopyToClipboard } from "react-copy-to-clipboard";
// reactstrap components
import {
  Card,
  Button,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  CardTitle,
  UncontrolledTooltip,
  UncontrolledCarousel
} from "reactstrap";

import Headerguest from "components/Headers/Headerguest.js";
import python from '../../assets/img/brand/python.jpg';
import slide1 from '../../assets/img/brand/slide1.png';
import slide2 from '../../assets/img/brand/slide2.png';
import slide3 from '../../assets/img/brand/slide3.png';
const items = [
  {
    src: slide1,
    alt:"description of image",
    altText: '',
    caption: '',
    header: '',
    key: '1'
  },
  {
    src: slide2,
    altText: '',
    caption: '',
    header: '',
    key: '2'
  },
  {
    src: slide3,
    altText: '',
    caption: '',
    header: '',
    key: '3'
  }
];
class HomeView extends React.Component {
  state = {};
  render() {
    return (
      <>
        <Headerguest />
        {/* Page content */}
        <Container  style={{ backgroundColor: '#F8F9FE',marginTop: '-30px' }} fluid>
          <div className="header-body">
              <Row >
                <Col lg="6" xl="3"></Col>
                <Col lg="6" xl="3" className="col text-center">
                    <Button href="/guest/register" style={{ backgroundColor: '#19BBAE', borderColor: '#19BBAE', width : "20vw" }} className="card-stats mb-4 mb-xl-0" >
                      <Row>
                        <div className="col " >
                          <span style={{ color: "white",fontSize:"1.5vw",textAlign: "center"  }} className="h2 font-weight-bold mb-0" >REGISTER NOW </span>
                        </div>
                      </Row>
                    </Button>
                </Col>
                <Col lg="6" xl="3" className="col text-center">
                <Button href="/guest/login" style={{ backgroundColor: '#21231E', borderColor: '#21231E', width : "20vw" }} className="card-stats mb-4 mb-xl-0" >
                      <Row>
                        <div className="col " style={{ textAlign: "center" }}>
                          <span style={{ color: "white",fontSize:"1.5vw"  }} className="h2 font-weight-bold mb-0" >SIGN IN </span>
                        </div>
                      </Row>
                </Button>
                </Col>
              </Row>
            </div>
        </Container>

        <Container  fluid style={{ backgroundColor: '#F8F9FE' }}>
          <Row>
            <div className=" col" style={{ marginTop: '50px' }}>
              <Row  style={{  marginLeft:"7%",marginRight:"7%" }}>
                <Col lg="6" md="12">
                  <card>
                    <h1 style={{ fontSize:"3vw" }} className="title">Python</h1>
                    <h5 style={{ fontSize:"1vw" }}className="description">
                    Learn Python in the most social and fun way, with Eduplay !
                    Learn Python, one of today's most in-demand programming languages on-the-go, while playing, for FREE! Compete and collaborate with your fellow SoloLearners, while surfing through short lessons and fun quizzes. Practice writing Python code within the app, collect points, and show off your skills. 
                    When you complete the course, you'll win a Certificate of Completion as a trophy !
                    <br/>So don’t wait  dive right in! Start coding with Python !
                    <br/>
                    </h5>
                    <Button
                    style={{ backgroundColor: "#19BBAE",color: "white", borderColor: '#19BBAE' }}
                      href="/nucleo-icons"
                      size="lg"
                      target="_blank"
                    >
                      Start Now !
                    </Button>
                  </card>
                </Col>
                <Col className="col text-center" lg="6" md="12">
                  <card>
                    <img style={{
                      marginTop:"2%",
                      Height: "100%",
                      width : "100%"
                    }}src={python}></img>
                  </card>
                </Col>
             </Row>
            </div>
          </Row>
        </Container>

        <br/><br/><br/>

        <Container  fluid style={{ backgroundColor: '#EAEDF1' }}>
          <Row>
            <div className=" col" style={{ marginTop: '5%',marginLeft:"10%",marginRight:"10%",marginBlockEnd: '5%' }}>
              <Row>
                <Col >
                  <card >
                    <h5 style={{ fontSize:"2vw",textAlign: "center" }} className="title">
                    “A journey of a thousand miles begins with a single step.  
                    Work for what you want, where you want to be and who you want to be. 
                    Success does not fall into your lap by doing nothing. 
                    With dedication and hard work, you can achieve anything. 
                    The beginning to the rest of your life starts here…”
                    </h5>
                  </card>
                </Col>
              </Row>
            </div>
          </Row>
        </Container>

        <div  style={{ backgroundColor: '#F8F9FE' }}>
          <br/><br/>
          <h1 style={{ fontSize:"3vw",textAlign: "center" }} >Why Eduplay works ?</h1>
          <br/><br/>
          <div style={{  marginLeft:"10%",marginRight:"10%" }}>
          <UncontrolledCarousel  items={items} />
          </div>
        </div>
        
        <br/><br/><br/>

        <Container  fluid style={{ backgroundColor: '#EAEDF1' }}>
          <Row>
            <div className=" col" style={{ marginTop: '2.5%',marginBlockEnd: '3%' }}>
              <Row>
                <Col >
                  <card >
                    <h5 style={{ fontSize:"3vw",textAlign: "center" }} className="title">
                    Become a member of our growing community !
                    </h5>
                    <div className="col text-center">
                      <Button 
                      style={{ 
                        backgroundColor: "#19BBAE",color: "white",width : "250px",borderColor: '#19BBAE' }}
                        href="/"
                        target="_blank"
                      >
                        Start Learning Now
                      </Button>
                    </div>
                  </card>
                </Col>
              </Row>
            </div>
          </Row>
        </Container>
        
      </>
    );
  }
}

export default HomeView;
