import React from "react";
// react component that copies the given text inside your clipboard
import { CopyToClipboard } from "react-copy-to-clipboard";
// reactstrap components
import {
  Card,
  Button,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  CardTitle,
  UncontrolledTooltip,
  UncontrolledCarousel,
  CardText,
  CardImg,
  CardSubtitle

} from "reactstrap";

import Header from "components/Headers/Header.js";
import python from '../../assets/img/brand/python.jpg';
import ListPython from '../../assets/img/brand/ListPython.png';
import slide1 from '../../assets/img/brand/slide1.png';
import slide2 from '../../assets/img/brand/slide2.png';
import slide3 from '../../assets/img/brand/slide3.png';
const items = [
  {
    src: slide1,
    alt:"description of image",
    altText: '',
    caption: '',
    header: '',
    key: '1'
  },
  {
    src: slide2,
    altText: '',
    caption: '',
    header: '',
    key: '2'
  },
  {
    src: slide3,
    altText: '',
    caption: '',
    header: '',
    key: '3'
  }
];
class CoursesView extends React.Component {
  state = {};
  render() {
    return (
      <>
        <Header/>
        <h1 style={{ fontSize:"3vw",marginTop: '5%'}} className="col text-center">COURSES</h1>
        {/* Page content */}
        <Container>
        <div>
      <Card style={{  marginTop: "50px",
                      Height: "400px",
                      width : "300px"
                    }}>
        <CardImg top style={{
                      Height: "350px",
                      width : "298px"
                    }} src={ListPython} alt="Card image cap" />
        <CardBody>
          
          <CardText>Python : with Eduplay ! Learn Python, one of today's most in-demand programming languages on-the-go, while playing, for FREE !</CardText>
          <Button>START</Button>
        </CardBody>
      </Card>
    </div>
    </Container>
        

        <br/><br/><br/>

        <Container  fluid style={{ backgroundColor: '#EAEDF1',marginTop: '-0px',height:"320px" }}>
          <Row>
            <div className=" col" style={{ marginTop: '50px' }}>
              <Row>
                <Col >
                  <card >
                    <h5 style={{ fontSize:"25px",textAlign: "center",marginLeft:"10%",marginRight:"10%",marginTop:"35px" }} className="title">
                    “A journey of a thousand miles begins with a single step.  
                    Work for what you want, where you want to be and who you want to be. 
                    Success does not fall into your lap by doing nothing. 
                    With dedication and hard work, you can achieve anything. 
                    The beginning to the rest of your life starts here…”
                    </h5>
                  </card>
                </Col>
              </Row>
            </div>
          </Row>
        </Container>

        <div  style={{ backgroundColor: '#F8F9FE' }}>
          <br/><br/>
          <h1 style={{ fontSize:"45px",textAlign: "center" }} >Why Eduplay works ?</h1>
          <br/><br/>
          <div style={{  marginLeft:"10%",marginRight:"10%" }}>
          <UncontrolledCarousel  items={items} />
          </div>
        </div>
        
        <br/><br/><br/>
        
        <Container  fluid style={{ backgroundColor: '#EAEDF1',marginTop: '-0px',height:"230px" }}>
          <Row>
            <div className=" col" style={{ marginTop: '50px' }}>
              <Row>
                <Col >
                  <card >
                    <h5 style={{ fontSize:"45px",textAlign: "center" }} className="title">
                    Become a member of our growing community !
                    </h5>
                    <div className="col text-center">
                      <Button 
                      style={{ 
                        backgroundColor: "#19BBAE",color: "white",width : "250px",borderColor: '#19BBAE' }}
                        href="/"
                        target="_blank"
                      >
                        Start Learning Now
                      </Button>
                    </div>
                  </card>
                </Col>
              </Row>
            </div>
          </Row>
        </Container>
        
      </>
    );
  }
}

export default CoursesView;
